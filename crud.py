from itertools import tee
from msilib.schema import Class
from sqlalchemy.orm import Session
import models, schemas

#Students
def get_students(db:Session):
    return db.query(models.Student).all()

def get_student(db: Session, student_id: int):
    return db.query(models.Student).filter(models.Student.id == student_id).first()

def get_student_by_cpf(db: Session, student_cpf: str):
    return db.query(models.Student).filter(models.Student.cpf == student_cpf).first()

def create_student(db: Session, student: schemas.StudentCreate):
    db_student = models.Student(cpf=student.cpf, nome=student.nome, data_aniversario=student.data_aniversario, rg=student.rg, orgao_expedidor=student.orgao_expedidor)
    db.add(db_student)
    db.commit()
    db.refresh(db_student)
    return db_student

#Courses
def get_courses(db:Session):
    return db.query(models.Course).all()

def get_course(db: Session, course_id: int):
    return db.query(models.Course).filter(models.Course.id == course_id).first()

def get_course_by_name(db: Session, course_name: str):
    return db.query(models.Course).filter(models.Course.nome == course_name).first()

def create_course(db: Session, course: schemas.CourseCreate):
    db_course = models.Course(nome=course.nome, ano_criacao=course.ano_criacao, predio=course.predio)
    db.add(db_course)
    db.commit()
    db.refresh(db_course)
    return db_course

def get_course_students(db: Session, course_id: int):
    return db.query(models.Student).filter(models.Student.curso_id == course_id).all()

#Teachers
def get_teachers(db:Session):
    return db.query(models.Teacher).all()

def get_teacher(db: Session, teacher_id: int):
    return db.query(models.Teacher).filter(models.Teacher.id == teacher_id).first()

def get_teacher_by_cpf(db: Session, teacher_cpf: str):
    return db.query(models.Teacher).filter(models.Teacher.cpf == teacher_cpf).first()

def create_teacher(db: Session, teacher: schemas.TeacherCreate):
    db_teacher = models.Teacher(cpf=teacher.cpf ,nome=teacher.nome, titulacao=teacher.titulacao)
    db.add(db_teacher)
    db.commit()
    db.refresh(db_teacher)
    return db_teacher

#Classes
def get_classes(db:Session):
    return db.query(models.Class).all()

def get_class(db: Session, class_id: int):
    return db.query(models.Class).filter(models.Class.id == class_id).first()

def get_class_by_name(db: Session, class_name: str):
    return db.query(models.Class).filter(models.Class.nome == class_name).first()

def create_class(db: Session, classe: schemas.ClassCreate):
    db_class = models.Class(nome=classe.nome, descricao=classe.descricao, codigo=classe.codigo)
    db.add(db_class)
    db.commit()
    db.refresh(db_class)
    return db_class

def get_class_students(db: Session, class_id: int):
    return db.query(models.Student).filter(models.Student.id == models.StudentClass.aluno_id).filter(models.StudentClass.disciplina_id == class_id).all()