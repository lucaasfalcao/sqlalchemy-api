from sqlalchemy import Column, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship
from database import Base

class Student(Base):
    __tablename__ = "alunos"

    id = Column(Integer, primary_key=True, autoincrement=True)
    cpf = Column(String, unique=True)
    nome = Column(String)
    data_aniversario = Column(String)
    rg = Column(String)
    orgao_expedidor = Column(String)
    curso_id = Column(Integer, ForeignKey("cursos.id")) 

    curso = relationship("Course", back_populates="")
    disciplinas = relationship("Class", \
     secondary='aluno_disciplina', back_populates="alunos")

class Course(Base):
     __tablename__ = "cursos"

     id = Column(Integer, primary_key=True, autoincrement=True)
     nome = Column(String, unique=True)
     ano_criacao = Column(Integer)
     predio = Column(String)
     coordenador_id = Column(Integer, ForeignKey("professores.id"))

     coordenador = relationship("Teacher", back_populates="cursos")
     alunos = relationship("Student", back_populates="curso")

class Teacher(Base):
     __tablename__ = "professores"

     id = Column(Integer, primary_key=True, autoincrement=True)
     cpf = Column(String, unique=True)
     nome = Column(String)
     titulacao = Column(String)

     cursos = relationship("Course", back_populates="coordenador")
     disciplinas = relationship("Class", back_populates="professor")

class Class(Base):
     __tablename__ = "disciplinas"

     id = Column(Integer, primary_key=True, autoincrement=True)
     nome = Column(String, unique=True)
     descricao = Column(String)
     codigo = Column(String)
     professor_id = Column(Integer, ForeignKey("professores.id"))

     professor = relationship("Teacher", back_populates="disciplinas")
     alunos = relationship("Student", \
          secondary='aluno_disciplina', back_populates="disciplinas")

class StudentClass(Base):
     __tablename__ = "aluno_disciplina"
     __table_args__ = (
          UniqueConstraint('aluno_id', 'disciplina_id'),
     )

     id = Column(Integer, primary_key=True, autoincrement=True)

     aluno_id = Column(Integer, ForeignKey("alunos.id"))
     disciplina_id = Column(Integer, ForeignKey("disciplinas.id"))


    
