from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
import uvicorn
import crud, schemas, models
from database import get_db

app =FastAPI(title="PROGRAD API with SQLAlchemy")

#Students
@app.get(
    "/students",
    summary="Fetch all students data",
    tags=['Students'],
    response_model=list[schemas.Student],
)
def read_students(db: Session = Depends(get_db)):
    students = crud.get_students(db)
    return students

@app.get(
    "/students/{student_id}",
    summary="Fetch student data",
    tags=['Students'],
    response_model=schemas.Student,
)
def read_student(student_id: int, db: Session = Depends(get_db)):
    db_student = crud.get_student(db, student_id=student_id)
    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")
    return db_student

@app.post(
    "/students",
    summary='Add a new student',
    tags=['Students'],
    response_model=schemas.Student,
)
def create_student(student: schemas.StudentCreate, db: Session = Depends(get_db)):
    db_student = crud.get_student_by_cpf(db, student_cpf=student.cpf)
    if db_student:
        raise HTTPException(status_code=400, detail="CPF already registered.")
    return crud.create_student(db=db, student=student)

@app.put(
    '/students/{student_id}/course/{course_id}',
    summary="Set or change student's course",
    tags=['Students'],
    response_model=schemas.Student,
)
def change_student_course(student_id: int, course_id: int, db: Session = Depends(get_db)):
    student = read_student(student_id, db)
    read_course(course_id, db)
    db.query(models.Student)\
        .filter(models.Student.id == student_id)\
            .update({'curso_id': course_id})
    db.commit()
    db.refresh(student)
    return student

@app.post(
    '/students/{student_id}/class/{class_id}',
    summary='Add student to a class',
    tags=['Students'],
    response_model=schemas.Student,
)
def add_student_class(student_id: int, class_id: int, db: Session = Depends(get_db)):
    student = read_student(student_id, db)
    read_class(class_id, db)
    student_class = models.StudentClass(aluno_id=student_id, disciplina_id=class_id)
    db.add(student_class)
    db.commit()
    db.refresh(student)
    return student

@app.delete(
    '/students/{student_id}/class/{class_id}',
    summary='Remove student from a class',
    tags=['Students'],
    response_model=schemas.Student,
)
def rem_student_class(student_id: int, class_id: int, db: Session = Depends(get_db)):
    student = read_student(student_id, db)
    read_class(class_id, db)
    db.query(models.StudentClass)\
        .filter(models.StudentClass.aluno_id == student_id)\
            .filter(models.StudentClass.disciplina_id == class_id)\
                .delete()
    db.commit()
    db.refresh(student)
    return student

#Courses
@app.get(
    "/courses",
    summary="Fetch all courses data",
    tags=['Courses'],
    response_model=list[schemas.Course],
)
def read_courses(db: Session = Depends(get_db)):
    courses = crud.get_courses(db)
    return courses

@app.get(
    "/courses/{course_id}",
    summary="Fetch course data",
    tags=['Courses'],
    response_model=schemas.Course,
)
def read_course(course_id: int, db: Session = Depends(get_db)):
    db_course = crud.get_course(db, course_id=course_id)
    if db_course is None:
        raise HTTPException(status_code=404, detail="Course not found.")
    return db_course

@app.post(
    "/courses",
    summary='Add a new course',
    tags=['Courses'],
    response_model=schemas.Course,
)
def create_course(course: schemas.CourseCreate, db: Session = Depends(get_db)):
    db_course = crud.get_course_by_name(db, course_name=course.nome)
    if db_course:
        raise HTTPException(status_code=400, detail="Course name already registered.")
    return crud.create_course(db=db, course=course)

@app.put(
    '/courses/{course_id}/coordinator/{teacher_id}',
    summary="Set or change a teacher as the course coordinator",
    tags=['Courses'],
    response_model=schemas.Course,
)
def change_course_coordinator(course_id: int, teacher_id: int, db: Session = Depends(get_db)):
    course = read_course(course_id, db)
    read_teacher(teacher_id, db)
    db.query(models.Course)\
        .filter(models.Course.id == course_id)\
            .update({'coordenador_id': teacher_id})
    db.commit()
    db.refresh(course)
    return course

@app.get(
    "/courses/{course_id}/students",
    summary="Fetch students of course",
    tags=['Courses'],
    response_model=list[schemas.Student],
)
def read_course_students(course_id: int, db: Session = Depends(get_db)):
    students = crud.get_course_students(db, course_id=course_id)
    if students is None:
        raise HTTPException(status_code=404, detail="Course students not found.")
    return students

#Teachers
@app.get(
    "/teachers",
    summary="Fetch all teachers data",
    tags=['Teachers'],
    response_model=list[schemas.Teacher],
)
def read_teachers(db: Session = Depends(get_db)):
    teachers = crud.get_teachers(db)
    return teachers

@app.get(
    "/teachers/{teacher_id}",
    summary="Fetch teacher data",
    tags=['Teachers'],
    response_model=schemas.Teacher,
)
def read_teacher(teacher_id: int, db: Session = Depends(get_db)):
    db_teacher = crud.get_teacher(db, teacher_id=teacher_id)
    if db_teacher is None:
        raise HTTPException(status_code=404, detail="Teacher not found.")
    return db_teacher

@app.post(
    "/teachers",
    summary='Add a new teacher',
    tags=['Teachers'],
    response_model=schemas.Teacher,
)
def create_teacher(teacher: schemas.TeacherCreate, db: Session = Depends(get_db)):
    db_teacher = crud.get_teacher_by_cpf(db, teacher_cpf=teacher.cpf)
    if db_teacher:
        raise HTTPException(status_code=400, detail="CPF already registered.")
    return crud.create_teacher(db=db, teacher=teacher)

#Classes
@app.get(
    "/classes",
    summary="Fetch all classes data",
    tags=['Classes'],
    response_model=list[schemas.Class],
)
def read_classes(db: Session = Depends(get_db)):
    classes = crud.get_classes(db)
    return classes

@app.get(
    "/classes/{class_id}",
    summary="Fetch class data",
    tags=['Classes'],
    response_model=schemas.Class,
)
def read_class(class_id: int, db: Session = Depends(get_db)):
    db_class = crud.get_class(db, class_id=class_id)
    if db_class is None:
        raise HTTPException(status_code=404, detail="Class not found.")
    return db_class

@app.post(
    "/classes",
    summary='Add a new class',
    tags=['Classes'],
    response_model=schemas.Class,
)
def create_class(classe: schemas.ClassCreate, db: Session = Depends(get_db)):
    db_class = crud.get_class_by_name(db, class_name=classe.nome)
    if db_class:
        raise HTTPException(status_code=400, detail="Class name already registered.")
    return crud.create_class(db=db, classe=classe)

@app.put(
    '/classes/{class_id}/teacher/{teacher_id}',
    summary="Set or change a teacher as the class teacher",
    tags=['Classes'],
    response_model=schemas.Class,
)
def change_class_teacher(class_id: int, teacher_id: int, db: Session = Depends(get_db)):
    classe = read_class(class_id, db)
    read_teacher(teacher_id, db)
    db.query(models.Class)\
        .filter(models.Class.id == class_id)\
            .update({'professor_id': teacher_id})
    db.commit()
    db.refresh(classe)
    return classe

@app.get(
    "/classes/{class_id}/students",
    summary="Fetch students of class",
    tags=['Classes'],
    response_model=list[schemas.Student],
)
def read_class_students(class_id: int, db: Session = Depends(get_db)):
    students = crud.get_class_students(db, class_id=class_id)
    if students is None:
        raise HTTPException(status_code=404, detail="Class students not found.")
    return students


if __name__ == '__main__':
    uvicorn.run(app='main:app', port=3000, reload=True)