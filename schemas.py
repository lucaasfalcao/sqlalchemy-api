from pydantic import BaseModel, Field
from datetime import date
from typing import List

def snake_to_camel(snake_str: str | None = None):
    if snake_str is None:
        return None
    splitted = snake_str.split('_')
    return splitted[0] + ''.join(s.title() for s in splitted[1:])


class GenericModel(BaseModel):
    class Config:
        alias_generator = snake_to_camel
        allow_population_by_field_name = True


class StudentBase(GenericModel):
    cpf: str = Field(title="CPF of student", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    nome: str = Field(title='Name of student')
    data_aniversario: date | None = Field(default=None, title='Birth date of student')
    rg: str | None = Field(title='Number of identity card')
    orgao_expedidor: str | None = Field(title='Expediter of identity card')

    class Config:
        schema_extra = {
            "example": {
                "cpf":  "000.000.000-00",
                "nome": "Jane Smith",
                "data_aniversario": "1970-01-01",
                "rg": "0000000-0",
                "orgao_expedidor": "SSP"
                }
            }

class StudentCreate(StudentBase):
    pass


class CourseBase(GenericModel):
    nome: str = Field(title='Name of Course')
    ano_criacao: int = Field(title='Year of course creation')
    predio: str = Field(title='Course building name')

    class Config:
        schema_extra = {
            "example": {
                "nome": "Engenharia Civil",
                "ano_criacao": "1987",
                "predio":  "CTEC"
                }
            }

class CourseCreate(CourseBase):
    pass


class TeacherBase(GenericModel):
    cpf: str = Field(title="CPF of teacher", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    nome: str  = Field(title='Name of teacher')
    titulacao: str = Field(title='Teacher titration')

    class Config:
        schema_extra = {
            "example": {
                "cpf": "000.000.000-00",
                "nome": "Roberto",
                "titulacao": "Doutorado"
                    }
                }

class TeacherCreate(TeacherBase):
    pass

class Teacher(TeacherBase):
    id: int = Field(title='ID of teacher')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "cpf": "000.000.000-00",
                "nome": "Roberto",
                "titulacao": "Doutorado",
                "id": 0
                    }
                }


class ClassBase(GenericModel):
    nome: str = Field(title='Name of class')
    descricao: str = Field(title='Description of class')
    codigo: str = Field(title='Code of class')

    class Config:
        schema_extra = {
            "example": {
                "nome": "Cálculo 1",
                "descricao": "Limites, derivadas, integrais, etc",
                "codigo": "MAT-01"
                    }
                }

class ClassCreate(ClassBase):
    pass

class Class(ClassBase):
    id: int = Field(title='ID of class')
    professor: Teacher | None = Field(title='Class teacher')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "nome": "Cálculo 1",
                "descricao": "Limites, derivadas, integrais, etc",
                "codigo": "MAT-01",
                "id": 0,
                "professor": {
                    "cpf": "000.000.000-00",
                    "nome": "Roberto",
                    "titulacao": "Doutorado",
                    "id": 0
                }
            }
        }


class Course(CourseBase):
    id: int = Field(title='ID of course')
    coordenador: Teacher | None = Field(title='Coordinator of the course')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "nome": "Engenharia Civil",
                "ano_criacao": "1987",
                "predio":  "CTEC",
                "id": 0,
                "coordenador": {
                    "cpf": "000.000.000-00",
                    "nome": "Barbirato",
                    "titulacao": "Doutorado",
                    "id": 0
                }
            }
        }


class Student(StudentBase):
    id: int = Field(title='ID of student')
    curso: Course | None = Field(title='Course of the student')
    disciplinas: list[Class] | None = Field(title='Classes of the student')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "cpf": "000.000.000-00",
                "nome": "Jane Smith",
                "data_aniversario": "1970-01-01",
                "rg": "0000000-0",
                "orgao_expedidor": "SSP",
                "id": 0,
                "curso": {
                    "nome": "Engenharia Civil",
                    "ano_criacao": "1987",
                    "predio": "CTEC",
                    "id": 0
                },
                "disciplinas": [
                    {
                        "nome": "Cálculo 1",
                        "descricao": "Limites, derivadas, integrais, etc",
                        "codigo": "MAT-01",
                        "id": 0
                    },
                    {
                        "nome": "Mecânica dos Sólidos",
                        "descricao": "Elasticidade, tração, flexão, etc",
                        "codigo": "ECIV-51",
                        "id": 0
                    }
                ]
            }
        }

class StudentClass(GenericModel):
    disciplina: Class | None = Field(title='Class')
    alunos: list[Student] | None = Field(title='Students of the class')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
